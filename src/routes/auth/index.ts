import { Router } from "express";
import { authRedirectController, consumeToken, discordLoginController, generateTempToken, getAuthenticatedUserController, revokeAccessTokenController } from "../../controllers/auth";

const router = Router();

router.get("/redirect", authRedirectController);
router.get("/user", getAuthenticatedUserController);
router.get("/revoke", revokeAccessTokenController);
router.get("/create", discordLoginController);
router.get("/tempToken", generateTempToken);
router.get("/consumeToken", consumeToken);

export default router;