export enum DISCORD_API_ROUTES {
    OAUTH2_TOKEN = "/oauth2/token",
    OAUTH2_USER = "/users/@me"
}

export type OAuth2ExchangeRequestParams = {
    client_id: string,
    client_secret: string,
    grant_type:  string,
    code: string,
    redirect_uri: string
}

export type CreateUserParams = {
    discordId: string;
    accessToken: string;
    refreshToken: string;
}