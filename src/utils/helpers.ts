import { AxiosRequestConfig } from "axios";
import url from "url";
import { OAuth2ExchangeRequestParams } from "./types";

export const buildOAuth2RequestPayload = (data: OAuth2ExchangeRequestParams) => new url.URLSearchParams(data).toString();

export const authHeaders = (accessToken: string): AxiosRequestConfig => ({
    headers: {
        Authorization: `Bearer ${accessToken}`
    }
});