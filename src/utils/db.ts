require("dotenv").config();
import { DataSource } from "typeorm";
import { User } from "../typeorm/entities/User";
import { TempToken } from "../typeorm/entities/TempToken";

const { DB_USER, DB_PASS, DB_NAME, DB_HOST, DB_PORT} = process.env;

const AppDataSource = new DataSource({
    type: "mysql", 
    host: DB_HOST,
    port: parseInt(DB_PORT!),
    username: DB_USER,
    password: DB_PASS,
    database: DB_NAME,
    entities: [ User, TempToken ],
    synchronize: true // TODO: set to false in prod
});

export default AppDataSource;