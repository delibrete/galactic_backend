require("dotenv").config();
import "reflect-metadata";
import express from 'express';
import routes from "./routes";
import sessions from "express-session";

const https = require('https');
const fs = require('fs');

var cors = require("cors");

import AppDataSource from "./utils/db";

const PORT = process.env.PORT!;

async function main() {
    const app = express();
    app.use(cors({
        origin: '*'
    })).use(sessions({
        secret: "secretKey",
        saveUninitialized:true,
        cookie: { maxAge: 1000 * 60 * 60 * 24 },
        resave: false 
    })).use("/api", routes);

    AppDataSource.initialize().then(() => {
        console.log("Initialised!");

        //app.listen(PORT, () => console.log(`Running on port ${PORT}`));

	https.createServer({
	  key: fs.readFileSync(__dirname + "/../../private.key", "utf8"),
	  cert: fs.readFileSync(__dirname + "/../../certificate.crt", "utf8")
	}, app).listen(PORT, () => console.log(`Running on port ${PORT}`));

    }).catch((err) => {
        console.error("Error during Data Source initialization", err)
    });
}

main();
