import axios from "axios";
import url from "url";

import { Request, Response } from "express";
import { createUser, exchangeAccessCodeForCredentials, getDiscordUserDetails, getUserById } from "../../services/auth";
import AppDataSource from "../../utils/db";
import { TempToken } from "../../typeorm/entities/TempToken";

const { DISCORD_API_URL, DISCORD_APP_CLIENT_ID, DISCORD_APP_CLIENT_SECRET, DISCORD_REDIRECT_URL } = process.env;

export async function authRedirectController (req: Request, res: Response) {
    console.log("discord redirect start")
    const tempTokenRepository = AppDataSource.getRepository(TempToken);

    const currentTempToken = await tempTokenRepository.findOneBy({ sessionId: req.sessionID })
    
    const { code } = req.query;
    console.log(req.sessionID)
    console.log(code)
    console.log(currentTempToken)
    console.log(code && currentTempToken)

    if (code && currentTempToken) {
        console.log("inside")
        try {
            console.log(0)
            const response = await exchangeAccessCodeForCredentials({
                client_id: DISCORD_APP_CLIENT_ID!,
                client_secret: DISCORD_APP_CLIENT_SECRET!,
                grant_type: "authorization_code",
                code: code.toString(),
                redirect_uri: DISCORD_REDIRECT_URL!
            });
            console.log(1)

            const { access_token, refresh_token } = response.data;
            console.log(2)
            const {data: user} = await getDiscordUserDetails(access_token);

            console.log(3)
            console.log(user.id);

            const newUser = await createUser({discordId: user.id, accessToken: access_token, refreshToken: refresh_token});
            console.log(4)
            currentTempToken.userId = user.id;

            console.log(5)
            tempTokenRepository.save(currentTempToken);

            console.log(6)
            // res.send(newUser);
            res.send("<html><body>Successfully authenticated with Discord. Please return to the app.</body><script>window.close();</script></html>");
        } catch (err) {
            res.sendStatus(400);
        }
    } else {
        res.sendStatus(400);
    }
    console.log("discord redirect end")
}

export async function getAuthenticatedUserController (req: Request, res: Response) {
    res.send(200);
}

export async function revokeAccessTokenController (req: Request, res: Response) {
    res.send(200);
}

export async function generateTempToken (req: Request, res: Response) {
    const token = Math.floor(Math.random() * (99999 - 10000 + 1) + 10000);
    res.send(token.toString());
}

export async function discordLoginController (req: Request, res: Response) {
    console.log("discordLoginController start")
    if (!req.query.token) return res.sendStatus(400);
    const tempTokenRepository = AppDataSource.getRepository(TempToken);
    const token = req.query.token as string;
    // const session = req.session;
    console.log(req.sessionID);
    console.log(token);

    const newTempToken = tempTokenRepository.create({
        tempToken: token,
        sessionId: req.sessionID
    });

    tempTokenRepository.save(newTempToken);

    // res.send("redirecting to discord login");
    // localhost
    // res.redirect("https://discord.com/api/oauth2/authorize?client_id=1004850029594431640&redirect_uri=http%3A%2F%2Flocalhost%3A3001%2Fapi%2Fauth%2Fredirect&response_type=code&scope=identify")
    // network
    // res.redirect("https://discord.com/api/oauth2/authorize?client_id=1004850029594431640&redirect_uri=http%3A%2F%2F192.168.1.161%3A3001%2Fapi%2Fauth%2Fredirect&response_type=code&scope=identify");
    // aws
    // res.redirect("https://discord.com/api/oauth2/authorize?client_id=1004850029594431640&redirect_uri=http%3A%2F%2Fec2-34-234-88-160.compute-1.amazonaws.com%3A3001%2Fapi%2Fauth%2Fredirect&response_type=code&scope=identify");
    // aws ssl
    res.redirect("https://discord.com/api/oauth2/authorize?client_id=1004850029594431640&redirect_uri=https%3A%2F%2F34.234.88.160.nip.io%3A3001%2Fapi%2Fauth%2Fredirect&response_type=code&scope=identify");
    console.log("discordLoginController end")
}

export async function consumeToken (req: Request, res: Response) {
    if (!req.query.token) return res.sendStatus(400);
    const token = req.query.token as string;

    console.log(token)

    const tempTokenRepository = AppDataSource.getRepository(TempToken);

    const userFromTempToken = await tempTokenRepository.findOneBy({ tempToken: token });

    if (!userFromTempToken) return res.sendStatus(500);

    console.log(userFromTempToken);

    const user = await getUserById(userFromTempToken.userId.toString());

    if (!user) return res.sendStatus(500);

    console.log(user);

    await tempTokenRepository.remove(userFromTempToken);

    res.send(user);
}