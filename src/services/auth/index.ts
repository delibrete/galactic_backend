import axios from "axios";
import { axiosConfig } from "../../utils/constants";
import { buildOAuth2RequestPayload, authHeaders } from "../../utils/helpers";
import { CreateUserParams, DISCORD_API_ROUTES, OAuth2ExchangeRequestParams } from "../../utils/types";
import { APIUser } from "discord-api-types/v10";
import AppDataSource from "../../utils/db";
import { User } from "../../typeorm/entities/User";

const { DISCORD_API_URL } = process.env;

export async function exchangeAccessCodeForCredentials(data: OAuth2ExchangeRequestParams) {
    const payload = buildOAuth2RequestPayload(data);
    return axios.post(DISCORD_API_URL + DISCORD_API_ROUTES.OAUTH2_TOKEN, payload, axiosConfig);
}

export async function getDiscordUserDetails(access_token: string) {
    return await axios.get<APIUser>(
        DISCORD_API_URL + DISCORD_API_ROUTES.OAUTH2_USER,
        authHeaders(access_token)
    );
}

export async function getUserById(id: string) {
    const userRepository = AppDataSource.getRepository(User);

    const userDB = await userRepository.findOneBy({ discordId: id });

    if (userDB) return userDB;

    return Promise.reject("No user with that id");
}

export async function createUser(params: CreateUserParams) {
    const userRepository = AppDataSource.getRepository(User);

    const userDB = await userRepository.findOneBy({ discordId: params.discordId });
    if (userDB) return userDB;

    const newUser = userRepository.create(params);
    return userRepository.save(newUser);
}

// This kind of sucks and not what we need. We're going to allow multiple sessions.
// export async function updateUser(user: User, params: CreateUserParams) {
//     const userRepository = AppDataSource.getRepository(User);

//     await userRepository.update({ discordId: user.discordId }, params);

//     // return 
// }