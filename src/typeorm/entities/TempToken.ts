import { tokenToString } from "typescript"
import { User } from "./User";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({name: "temp_tokens"})
export class TempToken {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: "temp_token", unique: true })
    tempToken: string;

    @Column({ name: "session_id", unique: true })
    sessionId: string;

    @Column({ name: "user_id", nullable: true })
    userId: string;
}